Trailhead 1
===========

Notable Frames
==============
![1](out0293.png)

```
AHtOplvXykF6mEZXFFc3jw
```
This is the name of the owner on google drive.

![2](out0294.png)

```
        JrOkOTYK
bWwyUCSQ
        EyhlWjOy
KknIeShi
```

![3](out0296.png)

```
        wJ3hg4MO
eiu6p4Uq
        2LzWEoum
bxWkbHIz
```

![4](out0298.png)

```
        u6MB6C3Q
hc2mEUsH
        jlRn6Awf
o8nAEmdF
```

![8](out0304.png)

`145,96`

![9](out0305.png)

`145,96`

![10](out0308.png)

```
https://bit.ly
      +
-... ...-- -.... .---- -. .---- -. -....
```
- <details><summary>See trails below for more details</summary>

    Morse code for:
    ```
    B361N1NB
    ```

    Leads to:
    https://drive.google.com/file/d/1SKz5FUtaCnvCbO43t2aJ6j2zkuhEWK9i/view?usp=sharing

 </details>


![11](out0310.png)

```
57  55
        59
57
    08
        05
46  47  07
```

![12](out0311.png)

```
132424132439

0 00 00 0  0  0 00 0  0
         0 000  00 0000
00   0     0         0
```
- <details><summary>Details inside, assuming this will arrive later</summary>

    Assuming:
    ```
    This is baudot code and will find hires later
    ```
 </details>

![13](out0313.png)

![14](out0314.png)

![15](out0316.png)

DATA CODE scans to: https://imdb.to/2CAhe4q

![16](out0395.png)

![17](out0508.png)

- Bottom is Japanese:
```
一四五九六
14596
```

- QR code top left:
  [Trail 2](../2/README.md)
  =========================

- QR code top right:
  [Trail 3](../3/README.md)
  =========================

<p><br><br><br>

![18](out0516.png)

Seems no different, just with RGB effect.
```
一四五九六
14596
```

UNUSED CLUES
============
Name of Author:
```
AHtOplvXykF6mEZXFFc3jw
```
```
        JrOkOTYK
bWwyUCSQ
        EyhlWjOy
KknIeShi
```
```
        wJ3hg4MO
eiu6p4Uq
        2LzWEoum
bxWkbHIz
```
```
        u6MB6C3Q
hc2mEUsH
        jlRn6Awf
o8nAEmdF
```
`14596`
