Trail 2
===========

[From: Trailhead 1](../1/README.md)
===================================

![17](../1/out0508.png)

- QR code top left:
  http://bit.ly/LnkPrK     
  "Linkin ParK"
  Leads to:<br>
  https://drive.google.com/file/d/1h3amokdbERIxn0XyMPlXlvJWjFEfuaBb/view?usp=sharing

  **forgive us** <br>
  ![forgive us](forgive%20us.png) <br>
  Appears to be simple book code.  Symbol of the band Linkin Park.

  `opSnothingHere`
  Code|Letter
  ----|--
  2:2 |o
  9:10|p
  3:1 |S
  10:3|n
  2:2 |o
  9:3 |t
  0:3 |h
  4:1 |I
  8:3 |n
  4:9 |g
  2:1 |H
  4:3 |e
  12:6|r
  1:6 |e

**oops, nothing here**
***Apparant dead-end***
-----------------------
