Trail 3
===========


[From: Trailhead 1](../1/README.md)
===================================

![1](../1/out0508.png)

QR code top right:
https://bit.ly/7h3F1r57
"The First"
Leads to:<br>
https://drive.google.com/file/d/1q2ipg3O2NDBqYbhnadRqgszvCcJoyjob/view?usp=sharing

**warming up.png**<br>
![warming up](warming%20up.png)
- https://bit.ly/B361N1N6
- https://drive.google.com/file/d/1SKz5FUtaCnvCbO43t2aJ6j2zkuhEWK9i/view?usp=sharing

  **Pingala.png**<br>
  ![Pingala](Pingala.png)
  - `01001100 00110011 00110011 00110111 01000110 00110000 00110000 00110111`
    - `L337F007`
    - http://bit.ly/L337F007

    **Lack.png**<br>
    ![Lack](Lack.png)
    - Added back in markers: <br>
    ![Lack-restored](Lack-restored.png)
      - Scans to: `Go back and remove L33 and add R16H`
      - http://bit.ly/R16H7F007
      - https://drive.google.com/file/d/1azPghv0JU5lI4aIvBpkOFAI1S7Y7c4yn/view?usp=sharing

        **AIZXXVI**<br>
        (A1Z26)<br>
        ![AIZXXVI](AIZXXVI.png)
        `11 1 20 15 14 7 15 21 11`<br>
        `katongouk`<br>
        - http://bit.ly/katongouk
        - https://drive.google.com/file/d/1xMBv5D3awIZ7NjQQ5OOoJFCkM8QV6Kk_/view?usp=sharing

          **+\_New_Zealand_**   <br>       
          Author: Corvus Tiberius III<br>
          ![+ New Zealand](+%20New%20Zealand%20.png)
          <br>
          Rearranged:<br>

          ![+ New Zealand repaired)](+%20New%20Zealand%20repaired.png) <br>
          `09 46 57`<br>
          `59 05 08`<br>
          `47 55 57`<br>

          Could be base64? <br>
          Dec|Base64
          ---|------
          09 |J
          46 |u
          57 |5
          59 |7
          05 |F
          08 |I
          47 |v
          55 |3
          57 |5

          - https://bit.ly/Ju57FIv35
          - https://drive.google.com/file/d/1SrqiCFJ6zxfDAIXXochCm1K2DFnXgAWJ/view
            **programozási nyelvek** (programming languages) <br>
            ![programozási nyelvek](programozási%20nyelvek.png) <br>
            ```
            2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 5b 3e
            2b 3e 2b 2b 2b 3e 2b 2b 2b 2b 2b 2b
            2b 3e 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
            3c 3c 3c 3c 2d 5d 3e 3e 2b 2b 2b 2b
            2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
            2b 2b 2b 2b 2e 3e 3e 2d 2d 2d 2d 2d
            2d 2d 2d 2d 2d 2d 2d 2d 2d 2e 2d 2d
            2d 2d 2d 2d 2d 2e 2d 2d 2d 2d 2e 2b
            2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
            2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2e
            2d 2d 2d 2d 2d 2d 2d 2d 2d 2d 2d 2d
                 2d 2d 2e 3c 2b 2b 2b 2b 2b 2e
            ```

              `++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>++++++++++++++++++++.>>--------------.-------.----.++++++++++++++++++++++++.--------------.<+++++.`

              `2VOKcUK`

              http://bit.ly/2VOKcUK <br>
              https://drive.google.com/file/d/1Nap7kQyOmDB4O179b5uLKaV8E-5l2flI/view?usp=sharing <br>

              **Oncriptides.png** <br>
              (original image file name appears to be "burning brain.")
              Oncriptides -> On cripti des -> des cripti On -> description
              ![Oncriptides](Oncriptides.png)
              `00 73 31 82 82 11 50 29 676 356 19 166 056 34 29 29 30 066 766 166 166 956`

              Check the description on google drive:

              `605 256 395 156 805 495 956 935 295 676 356 195 166 056 345 295 295 305 766 166 166 956`

              Owner:
              `AHt0p1vXykF6mEZXFFc3jw` <br>

              **HINTS FROM IMAGE**
              Visible 10-gon: take numbers digit by digit
              hidden text: hint to read backwards (right to left)
              hidden pentagon: ROT-5 the digits by 5

              ```
              Reverse description:
              956 166 166 766 305 295 295 345 056 166 195 356 676 295 935 956 495 805 156 395 256 605


              h   t   t   p   :   /   /   b   i   t   .   l   y   /   T   h   1   5   j   0   k   3
              ```
                - http://bit.ly/Th15j0k3
                - https://drive.google.com/file/d/1gFQGMWpUHO2b9hB3MiZqjX0VKisRuJX2/view

              **it's everything ok?.txt**
              `Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook! Ook! Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook. Ook. Ook? Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook. Ook! Ook? Ook? Ook. Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook. Ook? Ook! Ook! Ook? Ook! Ook? Ook. Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook! Ook.`
              - Ook! code executes to: https://bitly.com/0h4y0u
                  - おはよう -> Good morning
              - https://drive.google.com/file/d/1vl9z8gXgqndfRwLCfJ2KhwnAhO6G8lT3/view?usp=sharing
                **Kirim.png** <br>
                ![Kirim](Kirim.png) <br>
                `AHt0p1vXykF6mEZXFFc3jw` (name of Author again) <br>
                  coordinates: <br>
                  37.422061, -122.084072 <br>
                  Google Building 40, Amphitheatre Parkway, Mountain View, CA 94043, United States of America <br>
                  ***name googol extra-large-hex @ google*** <br>

                  [Trail 4](../4/README.md)
                  =========================

              ```
              Reverse Oncriptides:
              956 166 166 766 066 30# 29# 29# 34# 056 166 19# 356 676 29# 50# 11# 82# 82# 31# 73# 00#
              h   t   t   p   s   :   /   /   b   i   t   .   l   y   /  <-_? B   I   I   D   R   7
              ```

              ```
              #ff0000
              auf
              deutsch
              ```
              Red plane:
              ![Oncriptides Red 1](results/Oncriptides.png_Red_1.png)

              Red in German (Read in German backwards)
