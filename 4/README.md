Trail 4
===========

[From: Trail 3](../3/README.md)
===================================

```
From: AHt0p1vXykF6mEZXFFc3jw@gmail.com
Very good, you did very well,you went through three levels of difficulty. From now on you will be tested with more difficult riddles with a gradual growth of difficulty.

 link to step 4 : http://bit.ly/N3wD4wN
 doubts, suggestions only by reddit : u/sonsoftiberius

                                                                     Good luck         
```                                                                     

http://bit.ly/N3wD4wN
https://drive.google.com/drive/folders/1ZyNc7bam7eTV_JdQ5iae69YyY38vRYde <br>
A folder called `step 4`:
  **Whyl696342.png**
  ------------------
  <br>Description: `tekau ma waru` <br>
  (maori for 18) <br>
      ***- This is a hint to tell us to use ROT 18 (checking 13 shows: July141897 ) which is 14 July 1897. This is a hint to when the Dorabella Cipher was sent.<br>***
  ![Whyl696342](Whyl696342.png)
  - This is a Dorabella Cipher
    ==========================
    https://www.dcode.fr/dorabella-cipher
    =====================================
    `nmywn`

    =====
    https://bit.ly/nmywn <br>
    https://drive.google.com/file/d/1FjHuUWd_njHfMHMlMDPC0iyXTqpiBixx/view?usp=sharing
      - **3rD cEnTuRy.png**
        ---------------------
        ![3rD cEnTuRy](3rD%20cEnTuRy.png) <br>

        - The Brahmi numerals are a numeral system attested from the 3rd century BCE (somewhat later in the case of most of the tens). They are the direct graphic ancestors of the modern Indian and Hindu–Arabic numerals.
        ![image](2000px-Brahmi_numeral_signs.svg.png)

        `33 777 66 2 22 66`

        multitap cipher:
        https://www.dcode.fr/multitap-abc-cipher
        `ernabn`
        https://bit.ly/ernabn
        https://drive.google.com/file/d/1NxOthHumjMeU3_SpZba2xKRouFNsewcm/view?usp=sharing

          - **part 1.gif**
            ----------------
            ![part 1.gif](part%201.gif)

            `aHR0cHM6Ly9iaXQu`

            ![](out01.png)![](out06.png)

            This will be the first half of the next link.


  **Burning Brain.png**
  ================================
  ---------------------
  ![Burning Brain](Burning%20Brain.png) <br>
  `132424132439`<br>  
    - Could be A1Z26:
      13 24 24 13 24 3 9
      M  X  X  M  X  C I
      1020     1091
  Alpha for:
  `⠥⠉⠍⠑⠗⠊⠛⠓⠞`<br>
  Decodes to: `ucmeright`
  **"You see me, right?"**
  ========================
  - Text after IEND:`\r\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     66 75 74 62 61 6c 6f 20 65 6e 20 6c 61 20 6c 61 74 69 6e 61`
    - `futbalo en la latina`
    - Novial or Esperanto for "Football in the Latin language"
    - Latin word is eu, pila, or morbi
      - morbi hints at morbit
        =====================
    -https://www.dcode.fr/morbit-cipher
    Ciphertext: 132424132439
    Keyword: UCMERIGHT

      https://bitly.com/argkrts
      https://drive.google.com/file/d/1fbTbIRG21Q9KnqpaQjBf9u2pQZDH7snZ/view?usp=sharing <br>

      **Zimno.png** <br>
      =============
      ![Zimno](Zimno.png) <br>

      **Scan as a barcode.** <br>
      lsfdtw <br>
      https://bitly.com/lsfdtw
      https://drive.google.com/file/d/1y4aKndT1jY3lZVPw9zptdY54Ia0yylR1/view?usp=sharing <br>

      **part 2.gif**
      ==============
      ![part 2.gif](part%202.gif) <br>

      ![](part%2020000.png)![](part%2020001.png)

    Putting it all together:
    ========================

    ![](out01.png)![](out06.png)![](part%2020000.png)![](part%2020001.png)

    `aHR0cHM6Ly9iaXQubHkvMlJMRnU3dg==` <br>
    decode from base64: <br>
    https://bit.ly/2RLFu7v <br>
    https://drive.google.com/file/d/1QgW0eXzgZGMUV99EIDJ3u3KG1e8EdO0E/view?usp=sharing <br>

    **wh47 15 my n4m3 ?.png**
    ==============
    Description: `C4P5 L0CK` <br>
    ![](wh47%2015%20my%20n4m3%20_.png) <br>

    This is the Voynich manuscript.  Follow the instructions to get the zip password finally: `V0YN1CH M4NU5CR1P7`

    **[step 5.rar](step%205.rar)**

    **[Trail 5](../5/README.md)**
    =========================
