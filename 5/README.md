Trail 5
===========

[From: Trail 4](../4/README.md)
===================================

  **Crow.png**
  ==============
  ![Crow](Crow.png) <br>

  `Xlir xsso xli sxliv, ew nywx ew jemv, Erh lezmrk tivletw xli fixxiv gpemq, Figeywi mx aew kvewwc erh aerxih aiev; Xlsykl ew jsv xlex xli tewwmrk xlivi Leh asvr xliq vieppc efsyx xli weqi,` <br>
  `Erh fsxl xlex qsvrmrk iuyeppc pec Mr pieziw rs wxit leh xvshhir fpego. Sl, M oitx xli jmvwx jsv ersxliv hec! Cix orsamrk lsa aec piehw sr xs aec, M hsyfxih mj M wlsyph iziv gsqi fego.` <br>

  Text at end of image: `Tiberius Claudius Caesar Augustus Germanicus`

  This hints at Caesar Cipher ROT 4.

  `Then took the other, as just as fair, And having perhaps the better claim, Because it was grassy and wanted wear; Though as for that the passing there Had worn them really about the same,` <br>
  `And both that morning equally lay In leaves no step had trodden black. Oh, I kept the first for another day! Yet knowing how way leads on to way, I doubted if I should ever come back.` <br>
  - "The Road Not Taken" by Robert Frost

Dice cipher: <br>
```
GG LE ET FI GI FT
     LE GG GI KGGI IP
GG LE ET FI KE   FF LE FF
     EG ET GI KEEE
```

Each pair of dots is a substitution. Treating this as a cryptogram, and using the hint crow, but in latin, we can get:

<br><br><br>
Password for zip file: <br>

`Corvus oculum corvi non eruit`

**[step 6.rar](step%206.rar)**

**[Trail 6](../6/README.md)**
=============================
