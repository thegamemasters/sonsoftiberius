Trail 6
===========

[From: Trail 5](../5/README.md)
===================================

  **Ryyn Zvaabj Crn.gif**
  ==============
  ![Ryyn Zvaabj Crn](Ryyn%20Zvaabj%20Crn.gif) <br>

  `Rynn Zvaabj Crn` <br>
  is <br>
  `Ella Minnow Pea` <br>

  Frame at end of image and hint refers to `The quick brown fox jumps over the lazy dog`


Password for zip file: <br>
=============================

`The quick brown fox jumps over the lazy dog`

**[step 7.rar](step%207.rar)**

**[Trail 7](../7/README.md)**
=============================
