Trail 7
===========

[From: Trail 6](../6/README.md)
===================================

  **Eobard Thawne.mp4**
  ==============
  ![Eobard Thawne](Eobard%20Thawne.mp4) <br>

  Eobard Thawne is the Reverse Flash.

  The video has hidden text (reversed) saying: `follow the link`

  Morse in the video is: `...-------......--....-..-.----..`

  When reversed: `..----.-..-....--......-------..`

  Separated by brute force:

  `..--- -.-. .-.. .. --.. .... ----- --...`

  `2CLIZH07`
  https://drive.google.com/drive/folders/1cYbHntzDohIKSqC5NdsQWGZnYAnAg-Fh?usp=sharing <br><br>
  http://bitly.com/2CLIZHn  <br>

  - **Google Drive Folder**
  =======================
  Owner   `AHt0p1vXykF6mEZXFFc3jw` <br>
  Modified  `Sep 29, 2020 by AHt0p1vXykF6mEZXFFc3jw`
  Title:    `Nihon no sūji`
    - (means `Japanese Numbers`)
  Comment:  `43 41 50 53 20 4c 4f 43 4b`
    - (ascii for `CAPS LOCK`)

  ![kyuu](kyuu.png) <br>
  ![hachi](hachi.png) <br>
  ![nana](nana.png) <br>
  ![roku](roku.png) <br>
  ![go](go.png) <br>
  ![yon](yon.png) <br>
  ![san](san.png) <br>
  ![ni](ni.png) <br>
  ![ichi](ichi.png) <br>

  put in order
  ==========================

  ![ichi](ichi.png)
  ![ni](ni.png)
  ![san](san.png)
  ![yon](yon.png)
  ![go](go.png)
  ![roku](roku.png)
  ![nana](nana.png)
  ![hachi](hachi.png)
  ![kyuu](kyuu.png)

  `314159265359`
  PI
  `SAN ICHI YON ICHI GO KYUU NI ROKU GO SAN GO KYUU`
  `SANICHIYONICHIGOKYUUNIROKUGOSANGOKYUU`

  TRY BACKWARDS
  `KYUU GO SAN GO ROKU NI KYUU GO ICHI YON ICHI SAN`
  `KYUUGOSANGOROKUNIKYUUGOICHIYONICHISAN`

  TRY FORWARDS
  `ICHI NI SAN YON GO ROKU NANA HACHI KYUU`
  `ICHINISANYONGOROKUNANAHACHIKYUU`


Password for zip file: <br>
=============================

``

**[step 8.rar](step%208.rar)**

**[Trail 8](../8/README.md)**
=============================
