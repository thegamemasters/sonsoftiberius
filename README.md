Sons of Tiberius
================

https://www.reddit.com/r/sleuths/comments/j46up6/a_small_puzzle_to_test_knowledge_in_several_areas/

```(A small puzzle to test knowledge in several areas, such as reasoning, logic, codes and others)
```

***Posted by u/sonsoftiberius***

***3 Oct 2020 1:12:22 UTC***

![Sons of Tiberius](1/jeed4u7lzkq51.mp4)

[Trailhead 1](1/README.md)
==========================
***unused clues***
  - [Trail 2](2/README.md)
    ======================
    ***dead-end***
  - [Trail 3](3/README.md)
    ======================
    ***open***
    - [Trail 4](4/README.md)
      ======================
      ***open***
